// import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'
import {Fragment, useEffect,useState} from 'react'

export default function Courses() {


	const [courses,setCourses] = useState([])

	useEffect(() => {
		fetch('https://secret-ravine-81905.herokuapp.com/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<CourseCard key = {course._id} courseProp = {course}/>
					)
			}))
		})

	},[])





			// console.log(coursesData)
			// console.log([0])

			return(	
			  <Fragment>	
				<h1>Courses</h1>
				{courses}
			  </Fragment>	


		)


}