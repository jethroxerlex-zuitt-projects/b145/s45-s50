import {useState,useEffect,useContext} from 'react'
import {useParams,useNavigate, Link} from 'react-router-dom'
import {Container,Row,Col,Card,Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
export default function CourseView(){

	const {user} = useContext(UserContext)

	const history = useNavigate()

	const {courseId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const enroll = (courseId) => {

		fetch('https://secret-ravine-81905.herokuapp.com/users/enroll',
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Enrolled Successfully",
					icon: "success",
					text: "Thank you for enrolling!"
				})

				history("/courses")

			}else{
				Swal.fire({
					title: "Enrolled Successfully",
					icon: "success",
					text: "Thank you for enrolling!"
				})
			}
		})
	}

	useEffect(() => {
		console.log(courseId)

		fetch(`https://secret-ravine-81905.herokuapp.com/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	},[courseId])


	return(
		<Container className="mt-5">
			
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description: </Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price: </Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30PM to 9:30 PM</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" onClick={()=> enroll(courseId)}>Enroll</Button>
								:

								<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>


		</Container>

	)
}