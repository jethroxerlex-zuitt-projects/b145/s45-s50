import {useState, useEffect} from 'react'
import { Card } from 'react-bootstrap';
import {Link} from 'react-router-dom'
export default function CourseCard({courseProp}) {

    // console.log(props)
    // console.log(typeof props)
    console.log(courseProp)

    //deconstruct the courseProp into their own variables
    const {name,description,price, _id} = courseProp

    // //syntax: const [getter,setter] = useState(initialValueofGetter)
    // const [count, setCount] = useState(0)
    //     // let seatCount = 30 - count
    // const [seats, seatCount] = useState(30)    
    // function enroll(){
    //     if (seats > 0) {
    //     setCount(count + 1)
    //     seatCount(seats - 1)
    //     console.log(`Enrollees: ${count}`)

    //     } 
    // }

    // useEffect(() =>{
    //     if(seats === 0) {
    //         alert(`No more seats available`)
    //     }
    // },[seats])

    return (
        <Card className= "mt-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                {/*<Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
