const coursesData = [
	{
		id: "wdc001",
		name: "PHP- Laravel",
		description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero consectetur dolore itaque consequuntur, quis quisquam molestiae repellat dignissimos cumque deserunt voluptatibus magni voluptas eum dolorum, tenetur natus explicabo beatae aliquid.",
		price: 45000,
		onOffer : true
	},

		{
		id: "wdc002",
		name: "Python- Django",
		description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero consectetur dolore itaque consequuntur, quis quisquam molestiae repellat dignissimos cumque deserunt voluptatibus magni voluptas eum dolorum, tenetur natus explicabo beatae aliquid.",
		price: 50000,
		onOffer : true
	},

		{
		id: "wdc003",
		name: "Java- Springboot",
		description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero consectetur dolore itaque consequuntur, quis quisquam molestiae repellat dignissimos cumque deserunt voluptatibus magni voluptas eum dolorum, tenetur natus explicabo beatae aliquid.",
		price: 55000,
		onOffer : true
	}


]

export default coursesData;